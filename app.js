const dotenv = require('dotenv');
dotenv.config();

const express = require('express');
const sqlite3 = require('sqlite3');
const { open } = require('sqlite');
const app = express();
const port = process.env.PORT | 3000;

app.set('view engine', 'ejs');
app.use(express.urlencoded({ extended: true }));

app.get('/', async (req, res) => {
  try {
    const db = await open({
      filename: 'mac_addresses.db',
      driver: sqlite3.Database,
    });

    var status = (req.query.status==undefined)?"":req.query.status;
    //console.debug(status);
    
    // Retrieve MAC addresses, statuses, and hostnames from mac_addresses table
    const macAddresses = await db.all('SELECT * FROM mac_addresses');

    // Retrieve MAC addresses and hostnames from device_info table
    const devices = await db.all('SELECT * FROM device_info');

    // Map MAC addresses to hostnames
    const macToHostname = {};
    for (const device of devices) {
      macToHostname[device.mac_address] = device.hostname;
    }

    // Update hostnames in macAddresses array if necessary
    for (const macAddress of macAddresses) {
      //if (macAddress.hostname === 'Unknown') {
        macAddress.hostname = macToHostname[macAddress.mac_address] || macAddress.hostname;
      //}
    }    
    res.render('mac_addresses', { macAddresses: macAddresses, status: status });
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error');
  }
});

app.get('/devices', async (req, res) => {
  try {
    const db = await open({
      filename: 'mac_addresses.db',
      driver: sqlite3.Database,
    });

    const devices = await db.all('SELECT * FROM device_info');

    res.render('device_info', { devices: devices});
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error');
  }
});

app.post('/adddevice', async (req, res) => {
  try {
    const { macAddress, hostname } = req.body;

    const db = await open({
      filename: 'mac_addresses.db',
      driver: sqlite3.Database,
    });

    await db.run(
      'INSERT INTO device_info (mac_address, hostname) VALUES (?, ?)',
      [macAddress, hostname]
    );

    res.redirect('/devices');
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error');
  }
});

app.post('/deletedevice', async (req, res) => {
  try {
    const { macAddress } = req.body;

    const db = await open({
      filename: 'mac_addresses.db',
      driver: sqlite3.Database,
    });

    await db.run('DELETE FROM device_info WHERE mac_address = ?', macAddress);

    res.redirect('/devices');
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error');
  }
});


app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});
