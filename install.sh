echo "Installing Python"
sudo apt install -y python3 python3-pip git

echo "Installing NodeJS"
curl -fsSL https://deb.nodesource.com/setup_20.x | sudo -E bash -
sudo apt install -y nodejs

echo "Upgrading npm"
sudo npm install npm -g

echo "Verify installations"
python3 -V
node --version

git clone https://gitlab.com/cmadera/local-network.git
cd local-network

echo "Install Python dependencies"
sudo pip install -r requirements.txt

echo "Setup PORT and IP Range"
echo PORT=3000 > .env
echo IP_RANGE="192.168.86.1/24" >> .env

echo "First run"
sudo python3 sniff.py

echo "Allows Python app (running as root) and NodeJS app (running as currect user) to write into the database file"
sudo chmod 777 mac_addresses.db

echo "Install NodeJS dependencies"
npm install

echo "Install PM2"
sudo npm install pm2 -g

echo "Run web application in background"
pm2 start app.js --name sniff

