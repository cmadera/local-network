# MAC Address Management System

This is a simple MAC Address Management System implemented using Python3, Node.js and SQLite. 
It allows you to track and manage MAC addresses and their associated hostnames on your home network.

By default the network is setup to verify the **ip_range = "192.168.86.1/24"** on the file **.env**.

The application monitor runs on port **3000**. You can change it on the **.env** file.

## Installation

0. Verify Python and NodeJS installations
```shell
# Install Python3 if you need
sudo apt install -y python3 python3-pip

# Install NodeJS 20 if you need
curl -fsSL https://deb.nodesource.com/setup_20.x | sudo -E bash -
sudo apt install -y nodejs

# Upgrade npm
sudo npm install npm -g

# Verify installations
python3 -V
node --version
```

1. Clone the repository:
```shell
git clone https://gitlab.com/cmadera/local-network.git
cd local-network
```
2. Get Python requirements and check sniff.py installation. It will create mac_address.db database
```shell
sudo pip install -r requirements.txt

sudo python3 sniff.py
```
3. Install NodeJS dependencies using npm:
```shell
npm install
```

4. Start NodeJS application
```shell
node app.js
```
Open your web browser and access the application at http://localhost:3000.

5. For background running use PM2
```shell
sudo npm install pm2 -g
pm2 start app.js --name sniff
```


6. To schedule sniff application to run every 5 minutes do this:
```shell
sudo crontab -e
```
Add the following line into the cron file:
```shell
*/5 * * * * /bin/sh /home/user/local-network/cron.sh
```

7. For full script run "install.sh" (it will only not add cron)
```shell
wget https://gitlab.com/cmadera/local-network/-/raw/main/install.sh
sh install.sh
```
