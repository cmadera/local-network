import scapy.all as scapy
import sqlite3
import socket
import os
from datetime import datetime
from dotenv import load_dotenv

load_dotenv()  # take environment variables from .env.

# Define the IP address range to scan
ip_range = os.getenv('IP_RANGE') # "192.168.86.1/24"
print("IP Range=" + ip_range)

# Create an ARP request packet to get the MAC address of the devices
arp_request = scapy.ARP(pdst=ip_range)

# Create an Ethernet frame to send the ARP request packet
broadcast = scapy.Ether(dst="ff:ff:ff:ff:ff:ff")

# Combine the Ethernet frame and the ARP request packet
arp_request_broadcast = broadcast / arp_request

# Send the combined packet and get the response
answered_list = scapy.srp(arp_request_broadcast, timeout=1, verbose=False)[0]

# Connect to the SQLite database
conn = sqlite3.connect('mac_addresses.db')
c = conn.cursor()

# Create a table to store the MAC addresses if it doesn't exist
c.execute('''CREATE TABLE IF NOT EXISTS mac_addresses
             (mac_address TEXT, ip_address TEXT, hostname TEXT, status TEXT, firsttimefound TEXT, lasttimefound TEXT)''')

# Create a table to store the MAC addresses if it doesn't exist
c.execute('''CREATE TABLE IF NOT EXISTS device_info
             (mac_address TEXT, hostname TEXT, status TEXT)''')

print("MAC Address\t\t\tIP Address")
print("-----------------------------------------------")
for element in answered_list:
    print(element[1].hwsrc + "\t" + element[1].psrc)


c.execute("UPDATE mac_addresses set status='Inactive'")

# Update the status of each MAC address
for element in answered_list:
    mac_address = element[1].hwsrc
    ip_address = element[1].psrc

    try:
        hostname = socket.gethostbyaddr(ip_address)[0]
    except socket.herror:
        hostname = 'Unknown'

    # Check if the MAC address already exists in the database
    c.execute("SELECT * FROM mac_addresses WHERE mac_address = ?", (mac_address,))
    existing_mac = c.fetchone()

    print(hostname)
    data_hora_atual = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    if existing_mac:
        c.execute("UPDATE mac_addresses set status='Active', ip_address=?, lasttimefound=? WHERE mac_address = ?", (ip_address, data_hora_atual, mac_address,))
    else:
        c.execute("INSERT OR REPLACE INTO mac_addresses VALUES (?, ?, ?, ?, ?, ?)", (mac_address, ip_address, hostname, "Active", data_hora_atual, data_hora_atual))

# Commit the changes and close the connection
conn.commit()
conn.close()
